# Ghost Builder

This is a project that builds Ghost Debian packages with [FPM](https://github.com/jordansissel/fpm/wiki) and releases to [packagecloud](https://packagecloud.io). The build script downloads the latest Ghost release, sets up proper directory structure and compiles this into a Debian package file.

## Setup

Install *node*, *unzip*, *FPM*, *package_cloud* and *node-pre-gyp*

Run as sudo:

```
curl -sL https://deb.nodesource.com/setup | bash -
apt-get install unzip nodejs ruby-dev build-essential
gem install fpm
gem install package_cloud
npm install node-pre-gyp -g
```

You need to log in to package_cloud to use the command. Run this command to force login:

```
package_cloud distro list deb
```

## Build and release a package

Once you have everything set up you're ready to roll. At first modfy build-deb.sh and set correct `PG_PROJECT` value. This should be in the form of `[your packagecloud username]/[packagecloud project]`

Run the build

```
./build.sh
```

## Install Ghost from your repo

Add the repository to your server (replace `andris9/ghost` with your own project and `trusty` with the ubuntu release you built the package in)

```
curl https://packagecloud.io/gpg.key | apt-key add -
add-apt-repository "deb https://packagecloud.io/andris9/ghost/ubuntu/ trusty main"
```

Make sure you have Node.js installed:

```
curl -sL https://deb.nodesource.com/setup | bash -
apt-get install nodejs
```

Once you have set up the repo, you can install Ghost with *apt-get*:

```
apt-get update
apt-get install ghost
```

### Configuring

By default the application starts listening on port 2368 automatically after the installation is finished. To change the port and other configuration, edit the configuration file at */etc/ghost.d/config.js*

Once you have changed the config file restart the app with:

```
initctl restart ghost
```

### File locations

Log file can be found at */var/log/ghost.log*

Configuration file can be found at */etc/ghost.d/config.js*

Content directory can be found at */var/lib/ghost*

Application files can be found at */opt/ghost*

### Upgrading

If you want to upgrade your current version you can do this with *apt-get*:

```
apt-get update
sudo apt-get install --only-upgrade ghost
```

## License

**MIT**