#!/bin/bash

set -e

PG_PROJECT="andris9/ghost"
DISTRO=$(lsb_release -c -s)

# Get the latest version
if [[ ! $GHOST_VERSION ]]; then
    GHOST_VERSION=`curl --silent "https://api.github.com/repos/TryGhost/Ghost/releases" | node get-version.js`
fi

# Check if we have already built this version
if [ -f dist/ghost-${GHOST_VERSION}_*.deb ]; then
    echo "Version ${GHOST_VERSION} already built"
    exit 0
fi

# Download release file
if [ ! -f downloads/ghost-${GHOST_VERSION}.zip ]; then
    wget "https://github.com/TryGhost/Ghost/releases/download/${GHOST_VERSION}/Ghost-${GHOST_VERSION}.zip" -O downloads/ghost-${GHOST_VERSION}.zip
fi

# Check if we have the release file
if [ ! -f downloads/ghost-${GHOST_VERSION}.zip ]; then
    echo "Was not able to download Ghost release file"
    return 1
fi

# Unzip Ghost release to opt/ghost
rm -rf opt/ghost
mkdir -p opt/ghost
unzip downloads/ghost-${GHOST_VERSION}.zip -d opt/ghost

# Move content folder to var/lib/ghost
rm -rf var/lib/ghost
mkdir -p var/lib
mv opt/ghost/content/ var/lib/ghost

# Install node modules
DIR=`pwd`
cd opt/ghost
npm install --production --unsafe-perm --no-bin-links --build-from-source
cd $DIR

# Ensure folder for distributions
mkdir -p dist
rm -rf dist/ghost-${GHOST_VERSION}_*.deb
fpm -s dir -t deb -n ghost -v ${GHOST_VERSION} -C . \
  -p dist/ghost-VERSION_ARCH.deb \
  --deb-upstart init/ghost \
  --config-files etc/ghost.d \
  -d "nodejs >= 0.10.0" \
  --description "Ghost – Just a blogging platform" \
  --before-remove "hooks/before-remove.sh" \
  --after-install "hooks/after-install.sh" \
  etc/ghost.d opt/ghost var/lib/ghost \
  && echo "Package successfully built"

# Push built package to packagecloud
package_cloud push ${PG_PROJECT}/ubuntu/${DISTRO} dist/ghost-${GHOST_VERSION}_*.deb