'use strict';

module.exports = {
    // ### Production
    // When running Ghost in the wild, use the production environment
    // Configure your URL and mail settings here
    production: {
        url: 'http://my-ghost-blog.com',

        mail: {},

        paths: {
            contentPath: '/var/lib/ghost/'
        },

        database: {
            client: 'sqlite3',
            connection: {
                filename: '/var/lib/ghost/data/ghost.db'
            },
            debug: false
        },

        server: {
            // Host to be passed to node's `net.Server#listen()`
            host: '0.0.0.0',
            // Port to be passed to node's `net.Server#listen()`, for iisnode set this to `process.env.PORT`
            port: '2368'
        }
    }
};