'use strict';

var data = '';

process.stdin.setEncoding('utf8');

process.stdin.on('readable', function() {
    var chunk = process.stdin.read();
    if (chunk !== null) {
        data += chunk;
    }
});

process.stdin.on('end', function() {
    JSON.parse(data).forEach(function(item){
        if(!item.prerelease && !item.draft && item.tag_name){
            console.log(item.tag_name);
            process.exit(0);
        }
    });
});