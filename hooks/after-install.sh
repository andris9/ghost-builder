#!/bin/bash

# Ensure www-ghost user
id -u www-ghost &>/dev/null || useradd -s /dev/null www-ghost
touch /var/log/ghost.log

chown -R www-ghost:www-ghost /var/lib/ghost
chown www-ghost:www-ghost /var/log/ghost.log

initctl start ghost || true


