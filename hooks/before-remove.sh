#!/bin/bash

initctl stop ghost || true

rm -rf /var/run/ghost.pid
rm -rf /var/log/ghost.log
